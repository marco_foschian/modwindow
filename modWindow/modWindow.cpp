// modWindow.cpp : definisce il punto di ingresso dell'applicazione console.
//

#include "stdafx.h"

void modWindow(HWND Handle, int left, int top, int wid, int hei)
{
	LONG lStyle = GetWindowLong( Handle, GWL_STYLE );
    lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
    SetWindowLong( Handle, GWL_STYLE, lStyle);
    LONG lExStyle = GetWindowLong( Handle, GWL_EXSTYLE );
    lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
    SetWindowLong( Handle, GWL_EXSTYLE, lExStyle );
    SetWindowPos( Handle, HWND_TOP, left, top, wid, hei,
		SWP_FRAMECHANGED
		| SWP_SHOWWINDOW
		| SWP_NOSENDCHANGING
		);
}

int getInt( _TCHAR* arg, int def_value )
{
	int v;
	int n = _stscanf_s( arg, _T("%d"), &v );
	if( n == 1 )
		return v;
	else
		return def_value;
}

struct Rect
{
	int left;
	int top;
	int width;
	int height;

	Rect() : left(0), top(0), width(640), height(480)
	{}

	Rect( int l, int t, int w, int h ) : left(l), top(t), width(w), height(h)
	{}
};

struct Options
{
	bool wait_window;
	int wait_time;
	Rect w;
	_TCHAR*	window_name;
	bool hide_mouse;
	bool enum_windows;
	int hide_mouse_x;
	int hide_mouse_y;
};



_TCHAR* PROGRAM_NAME = NULL;

HWND findWindow( _TCHAR* name )
{
	return ::FindWindow( NULL, name );
}

BOOL CALLBACK printWindow( HWND hwnd, LPARAM lParam )
{
	WINDOWINFO wi;
	wi.cbSize = sizeof(wi);

	BOOL ok = ::GetWindowInfo( hwnd, &wi );
	if( ok == FALSE )
		return TRUE;

	//DWORD searchedStyle = WS_OVERLAPPEDWINDOW;
	DWORD searchedStyle = WS_TILEDWINDOW | WS_SIZEBOX | WS_TABSTOP | WS_VISIBLE;
	DWORD refusedStyles = WS_CHILD;
	if( !(wi.dwStyle & searchedStyle) )
		return TRUE;

	if( wi.dwStyle & refusedStyles )
		return TRUE;

	_TCHAR title[101];
	int l = ::GetWindowText( hwnd, title, 100 );
	if( l <= 0 )
		return TRUE;

	wprintf( _T("\n%s"), title );
	return TRUE;
}

void enumWindows()
{
	wprintf( _T("\nEnumerating windows:") );
	//BOOL b = ::EnumWindows( printWindow, NULL );
	BOOL b = ::EnumDesktopWindows( NULL, printWindow, NULL );
	wprintf( _T("\n-------------\n") );
}

void help()
{
	wprintf( _T("\nUsage: %s <window name> [/l left] [/w width] [/t top] [/h height] [/wait seconds] [/enum] [/hidemouse] [/x px] [/y py] [/help|/?]\n"), PROGRAM_NAME );
}

int _tmain(int argc, _TCHAR* argv[])
{
	PROGRAM_NAME = argv[0];

	if( argc < 2 )
	{
		wprintf( _T("\nNo window specified") );
		help();
		return 1;
	}

	int screen_cx = GetSystemMetrics(SM_CXSCREEN);
	int screen_cy = GetSystemMetrics(SM_CYSCREEN);
	wprintf( _T("\nScreen is %d x %d"), screen_cx, screen_cy);

	Options o;
	// Defaults
	// Window size to full screen
	o.w.left = 0;
	o.w.top = 0;
	o.w.width = screen_cx;
	o.w.height = screen_cy;
	// Do not wait for the window to appear
	o.wait_window = false;
	o.wait_time = 60; // seconds;
	// Do not hide the mouse
	o.hide_mouse = false;
	o.hide_mouse_x = -1;
	o.hide_mouse_y = -1;
	// Do not enum the top-level windows
	o.enum_windows = false;

	o.window_name = NULL;


	// Decode command line parameters
	_TCHAR* arg = NULL;
	_TCHAR* val = NULL;
	for( int i=1; i < argc; )
	{
		arg = argv[i++];
		if( i < argc )
			val = argv[i];

		if( _tcsicmp( _T("/wait"), arg )==0  )
		{
			o.wait_window = true;
			o.wait_time = getInt( val, 60 );
			if( o.wait_time < 0 )
				o.wait_time = 0;
			i++;
		}
		else if( _tcsicmp( _T("/name"), arg )==0  )	{ o.window_name = val;	i++; }
		else if( _tcsicmp( _T("/l"), arg )==0  )	{ o.w.left = getInt( val, o.w.left );	i++; }
		else if( _tcsicmp( _T("/t"), arg )==0  )	{ o.w.top = getInt( val, o.w.top );	i++; }
		else if( _tcsicmp( _T("/w"), arg )==0  )	{ o.w.width = getInt( val, o.w.width );	i++; }
		else if( _tcsicmp( _T("/h"), arg )==0  )	{ o.w.height = getInt( val, o.w.height );	i++; }
		else if( _tcsicmp( _T("/enum"), arg )==0  )	{ o.enum_windows = true;}
		else if( _tcsicmp( _T("/hidemouse"), arg )==0  )	{ o.hide_mouse = true;}
		else if( _tcsicmp( _T("/x"), arg )==0  )	{ o.hide_mouse_x = getInt( val, -1 );	i++; }
		else if( _tcsicmp( _T("/y"), arg )==0  )	{ o.hide_mouse_y = getInt( val, -1 );	i++; }
		else if( _tcsicmp( _T("/help"), arg )==0 || _tcsicmp( _T("/?"), arg )==0  )
		{
			help();
			return 0;
		}
		else
		{
			// I suppose it should be the window name
			o.window_name = arg;
		}
	}

	if( o.hide_mouse )
	{
		int pos_x = screen_cx;
		if( o.hide_mouse_x >= 0 ) pos_x = o.hide_mouse_x;
		int pos_y = screen_cy / 2;
		if( o.hide_mouse_y >= 0 ) pos_y = o.hide_mouse_y;

		wprintf( _T("\nMoving mouse at %d, %d"), pos_x, pos_y);
		::SetCursorPos( pos_x, pos_y );
	}

	if( o.enum_windows )
	{
		// enum windows
		enumWindows();
		return 0;
	}

	if( o.window_name == NULL )
	{
		wprintf( _T("\nWindow name not specified"));
		help();
		return 1;
	}
	else
		wprintf( _T("\nWindow name is %s"), o.window_name );//argv 1

	if( o.wait_window && o.wait_time > 0 )
		wprintf( _T("\nWindow wait time set to %d"), o.wait_time );

	wprintf( _T("\nTry to set Window @ [%d,%d] with size [%d,%d]"), o.w.left, o.w.top, o.w.width, o.w.height );


	HWND handle = findWindow( o.window_name );
	if( handle == NULL && o.wait_window == true && o.wait_time > 0 )
	{
		wprintf(_T("\nWaiting for window"));
	}

	while( handle == NULL && o.wait_window == true && o.wait_time > 0 )
	{
		::Sleep( 1000 );
		wprintf(_T("."));
		o.wait_time--;
		handle = findWindow( o.window_name );
	}

	if( handle == NULL )
	{
		wprintf( _T("\nFailed to find %s\n"), o.window_name );
		return 1;
	}

	modWindow( handle, o.w.left, o.w.top, o.w.width, o.w.height );

	// Verify real window size
	RECT r;
	BOOL ok = ::GetWindowRect( handle, &r );
	if( ok )
		wprintf( _T("\nWindow is now @ [%d,%d] with size [%d,%d]"), r.left, r.top, r.right - r.left, r.bottom - r.top );
	else
		wprintf( _T("\nFailed to get window info") );

	wprintf( _T("\nDone\n") );
	return 0;
}
