This is a little tool to that move and resize a window also beyond the resolution and the boundaries of the desktop.

To use the tool you have to specify:

* the title of the window
* the position and the dimensions of the window
* a timeout (in seconds).

Once the program started it will search for a window with the specified name and, if none is found it will retry every second until the timeout is reached.
The name of the window is the content of the TITLE BAR.

The defaults will place the window full screen without wait timeout.

The syntax is:

modWindow window-name [/l left] [/w width] [/t top] [/h height] [/wait seconds] [/enum] [/hidemouse] [/x val] [/y val] [/help]

/enum simply print out the name of the top-most visible windows (experimental)
/hidemouse move the mouse to the right side of the desktop (at half the height) to make it "invisible" or at position /x /y if specified
/help shows the help screen

For Example:

modWindow "Google Earth" /l -640 /w 3200 /h 1200 /wait 60 /hidemouse

will make the Google Earth client window 3200 x 1200 pixels and put it 640 pixels out of the leftmost part of the desktop.
The top of the window remain zero.
The cursor is moved to the right side of the desktop, half the height.